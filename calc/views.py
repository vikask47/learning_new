from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Question
from django.template import loader


# Create your views here.
def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('calc/index.html')
    context = {'latest_question_list': latest_question_list}
    output = ', '.join(q.question_text for q in latest_question_list)

    return HttpResponse(template.render(context, request))
    # return render(request,'home.html')


def home2(request):
    return render(request, 'home.html')


def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not")
    return render(request, 'calc/detail.html', {'question': question})


def results(request, question_id):
    response = "You're looking at result of question {}."
    return HttpResponse(response.format(question_id))


def vote(request, question_id):
    return HttpResponse("You're voting on question {}".format(question_id))
